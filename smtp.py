# This file is part smtp module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import smtplib
from collections import namedtuple
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.transaction import Transaction

Response = namedtuple('Response', ['status_code'])

class SmtpConnectionMail(ModelSQL, ModelView):
    'SMTP Conection Mail'
    __name__ = 'smtp.connection.mail'
    name = fields.Char('Name', required=True)
    smtp_server = fields.Char('Server', required=True,
        states={
            'readonly': (Eval('state') != 'draft'),
            }, depends=['state'])
    smtp_port = fields.Integer('Port', required=True,
        states={
            'readonly': (Eval('state') != 'draft'),
            }, depends=['state'])
    smtp_ssl = fields.Boolean('SSL',
        states={
            'readonly': (Eval('state') != 'draft'),
            }, depends=['state'])
    smtp_tls = fields.Boolean('TLS',
        states={
            'readonly': (Eval('state') != 'draft'),
            }, depends=['state'])
    smtp_password = fields.Char('Password',
        states={
            'readonly': (Eval('state') != 'draft'),
            }, depends=['state'])
    smtp_email = fields.Char('Email', required=True,
        states={
            'readonly': (Eval('state') != 'draft'),
            }, depends=['state'],
        help='Default From (if active this option) and Reply Email')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('done', 'Done'),
            ], 'State', readonly=True, required=True)
    user = fields.Many2One('res.user', 'User')

    @classmethod
    def __setup__(cls):
        super(SmtpConnectionMail, cls).__setup__()
        cls._buttons.update({
                'get_smtp_test': {},
                'draft': {
                    'invisible': Eval('state') == 'draft',
                    },
                'done': {
                    'invisible': Eval('state') == 'done',
                    },
                })

    @classmethod
    def check_xml_record(cls, records, values):
        return True


    @staticmethod
    def default_smtp_ssl():
        return True

    @staticmethod
    def default_smtp_port():
        return 465

    @staticmethod
    def default_state():
        return 'draft'

    def get_uri(self):
        option = 'smtp'
        if self.smtp_tls:
            option = 'smtp+tls'
        elif self.smtp_ssl:
            option = 'smtps'
        # smtps://cuenta@dominio.com:miclave@mail.dominio.com:465
        return option + '://' + self.smtp_email + ':' + self.smtp_password + '@' + self.smtp_server + ':' + str(self.smtp_port)

    @classmethod
    @ModelView.button
    def draft(cls, servers):
        cls.write(servers, {
                'state': 'draft',
                })

    @classmethod
    @ModelView.button
    def done(cls, servers):
        cls.write(servers, {
                'state': 'done',
                })

    @classmethod
    @ModelView.button
    def get_smtp_test(cls, servers):
        """Checks SMTP credentials and confirms if outgoing connection works"""
        for server in servers:
            # try:
            server =  server.get_smtp_server()
            print(server, 'server')
            # except e:
            #     cls.raise_user_error('smtp_test_details', message)
            # except:
            #     cls.raise_user_error('smtp_error')
            # cls.raise_user_error('smtp_successful')

    def get_smtp_server(self):
        """
        Instanciate, configure and return a SMTP or SMTP_SSL instance from
        smtplib.
        :return: A SMTP instance. The quit() method must be call when all
        the calls to sendmail() have been made.
        """
        if self.smtp_ssl:
            smtp_server = smtplib.SMTP_SSL(self.smtp_server, self.smtp_port)
        else:
            smtp_server = smtplib.SMTP(self.smtp_server, self.smtp_port)

        if self.smtp_tls:
            smtp_server.starttls()

        if self.smtp_email and self.smtp_password:
            smtp_server.login(self.smtp_email, self.smtp_password)

        return smtp_server


class SMTPEmail():

    def prepare_attachment(self, data, filename, ext):
        if not filename:
            filename = 'test_filename'

        # encoded = base64.b64encode(data).decode()
        # attachment = Attachment()
        # attachment.file_content = FileContent(encoded)
        _filename = filename + '.' + ext
        # content_type, _ = mimetypes.guess_type(_filename)
        attachment.file_type = FileType(content_type)
        attachment.file_name = FileName(_filename)
        attachment.disposition = Disposition('attachment')
        attachment.content_id = ContentId('Example Content ID')
        return attachment

    def send(self, template, record, to_recipients=[], attach=False, attachments=[]):
        if not template or (not to_recipients and not template.recipients):
            return
        html_content = template.engine_jinja2(template.html_body, record, template.webapi.api_response)

        if not to_recipients:
            to_recipients_ = template.recipients
        else:
            to_recipients_ = ''
            for t in to_recipients:
                mail = ', ' + t
                to_recipients_ += mail
        Email = Pool().get('ir.email')
        user_id = template.webapi.smtp_conn.user.id
        user_transaction = Transaction().user
        with Transaction().set_user(user_id), Transaction().set_context({'user_transaction': user_transaction}):
            to = to_recipients
            bcc = template.bcc
            subject=template.subject
            body = html_content
            files = []
            record = []
            if record:
                record = str(record).split(',')
            else:
                record = str(template).split(',')
            for att in attachments:
                files.append((att['file_name'] + att['extension'], att['attachment']))
            reports = []
            if template.report:
                reports = [template.report.id]                
            Email.send(to=to_recipients_, cc='', bcc=bcc, subject=subject, body=body,
                files=files, record=record, reports=reports, attachments=None)

            return Response(status_code=202)

    def _test_send(self, template):
        # webservice = self.webapi.web_service
        # api = self.webapi.api_key
        Email = Pool().get('ir.email')
        html_content = template.html_body
        record = []
        if record:
            record = str(record).split(',')
        else:
            record = str(template).split(',')
        print(template.webapi.smtp_conn.user, 'user')
        with Transaction().set_user(template.webapi.smtp_conn.user.id):
            to = template.recipients
            bcc = template.bcc
            subject=template.subject
            body = html_content
            Email.send(to=to, cc='', bcc=bcc, subject=subject, body=body,
                files=[], record=record, reports=[], attachments=[])
        # message = Mail(
        #     from_email=template.from_email,
        #     to_emails='gomezwilson.99@gmail.com',
        #     subject=template.subject,
        #     plain_text_content='Quick and easy to do anywhere, even with Python',
        #     html_content=template.html_body
        # )
        # try:
        #     sg = SendGridAPIClient(self.api)
        #     response = sg.send(message)
        #     print(response.body)
        #     print(response.headers)
        # except Exception as e:
        #     print(e.message)

    # @classmethod
    # def get_smtp_server_from_model(self, model):
    #     """
    #     Return Server from Models
    #     :param model: str Model name
    #     return object server
    #     """
    #     model = Pool().get('ir.model').search([('model', '=', model)])[0]
    #     servers = Pool().get('smtp.server-ir.model').search([
    #             ('model', '=', model),
    #             ], limit=1)
    #     if not servers:
    #         self.raise_user_error('server_model_not_found', model.name)
    #     return servers[0].server


# class SmtpServerModel(ModelSQL):
#     'SMTP Server - Model'
#     __name__ = 'smtp.server-ir.model'
#     _table = 'smtp_server_ir_model'

#     server = fields.Many2One('smtp.server', 'Server', ondelete='CASCADE',
#         select=True, required=True)
#     model = fields.Many2One('ir.model', 'Model', ondelete='RESTRICT',
#         select=True, required=True)
