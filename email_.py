import mimetypes
from email.encoders import encode_base64
from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.nonmultipart import MIMENonMultipart
from email.mime.text import MIMEText
from email.utils import formataddr, getaddresses

try:
    import html2text
except ImportError:
    html2text = None

# from trytond.i18n import gettext
from trytond.config import config
from trytond.pool import Pool, PoolMeta
from trytond.ir.email_ import HTML_EMAIL, _get_emails
from trytond.sendmail import sendmail_transactional, SMTPDataManager
from trytond.transaction import Transaction
# from sendmail import _SMTPDataManager


class Email(metaclass=PoolMeta):
    "Email"
    __name__ = 'ir.email'

    @classmethod
    def send(cls, to='', cc='', bcc='', subject='', body='',
            files=None, record=None, reports=None, attachments=None):
        pool = Pool()
        User = pool.get('res.user')
        ActionReport = pool.get('ir.action.report')
        Attachment = pool.get('ir.attachment')
        user_id = Transaction().user
        user = User(user_id)
        with Transaction().set_user(0):
            user_ = User(user_id)
        SmtpConnection = pool.get('smtp.connection.mail')
        smtps = SmtpConnection.search([('user', '=', user.id)])
        signature = user_.signature

        uri = None
        user_email = None
        if smtps:
            uri = smtps[0].get_uri()
            user_email = smtps[0].smtp_email
        if record:
            Model = pool.get(record[0])
            record = Model(record[1])

        body_html = HTML_EMAIL % {
            'subject': subject,
            'body': body,
            'signature': signature or '',
            }
        content = MIMEMultipart('alternative')
        if html2text:
            body_text = HTML_EMAIL % {
                'subject': subject,
                'body': body,
                'signature': '',
                }
            converter = html2text.HTML2Text()
            body_text = converter.handle(body_text)
            if signature:
                body_text += '\n-- \n' + converter.handle(signature)
            part = MIMEText(body_text, 'plain', _charset='utf-8')
            content.attach(part)
        part = MIMEText(body_html, 'html', _charset='utf-8')
        content.attach(part)
        if files or reports or attachments:
            msg = MIMEMultipart('mixed')
            msg.attach(content)
            if files is None:
                files = []
            else:
                files = list(files)

            for report_id in (reports or []):
                report = ActionReport(report_id)
                Report = pool.get(report.report_name, type='report')
                ext, content, _, title = Report.execute(
                    [record.id], {
                        'action_id': report.id,
                        })
                name = '%s.%s' % (title, ext)
                if isinstance(content, str):
                    content = content.encode('utf-8')
                files.append((name, content))
            if attachments:
                files += [
                    (a.name, a.data) for a in Attachment.browse(attachments)]
            for name, data in files:
                mimetype, _ = mimetypes.guess_type(name)
                if mimetype:
                    attachment = MIMENonMultipart(*mimetype.split('/'))
                    attachment.set_payload(data)
                    encode_base64(attachment)
                else:
                    attachment = MIMEApplication(data)
                attachment.add_header(
                    'Content-Disposition', 'attachment',
                    filename=('utf-8', '', name))
                msg.attach(attachment)
        else:
            msg = content
        msg['From'] = from_ = user_email
        if user_.email:
            if user_.name:
                user_email = formataddr((user_.name, user_.email))
            else:
                user_email = user_.email
            msg['Behalf-Of'] = user_email
            msg['Reply-To'] = user_email
        msg['To'] = ', '.join(formataddr(a) for a in getaddresses([to]))
        msg['Cc'] = ', '.join(formataddr(a) for a in getaddresses([cc]))
        msg['Subject'] = Header(subject, 'utf-8')

        to_addrs = list(filter(None, map(
                    str.strip,
                    _get_emails(to) + _get_emails(cc) + _get_emails(bcc))))
        sendmail_transactional(
            from_, to_addrs, msg, datamanager=SMTPDataManager(uri=uri, strict=True))
        context = Transaction().context
        if context.get('user_transaction'):
            user_id = context.get('user_transaction')
        with Transaction().set_user(user_id):
            email = cls(
                recipients=to,
                recipients_secondary=cc,
                recipients_hidden=bcc,
                addresses=[{'address': a} for a in to_addrs],
                subject=subject,
                body=body,
                resource=record)
            email.save()
            with Transaction().set_context(_check_access=False):
                attachments_ = []
                for name, data in files:
                    attachments_.append(
                        Attachment(resource=email, name=name, data=data))
                Attachment.save(attachments_)
            return email
