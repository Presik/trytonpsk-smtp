# This file is part electronic_mail_template module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from .smtp import SMTPEmail


class Template(metaclass=PoolMeta):
    'Email Template'
    __name__ = 'email.template'

    @classmethod
    def send(cls, template, record, to_recipients=[], attach=False, attachments=[]):
        if not template or (not to_recipients and not template.recipients):
            return
        webservice = template.webapi.web_service
        api = template.webapi.api_key
        if webservice == 'smtp':
            # validate to send email with smtp
            smtpemail = SMTPEmail()
            return smtpemail.send(template, record, to_recipients, attach, attachments)
        else:
            return super(Template, cls).send(template, record, to_recipients, attach, attachments)

    def _test_send(self):
        webservice = self.webapi.web_service
        api = self.webapi.api_key
        provider = None
        if webservice == 'smtp':
            stmpemail = SMTPEmail()
            stmpemail._test_send(self)
        else:
            super(Template, self)._test_send()