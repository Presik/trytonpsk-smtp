# This file is part electronic_mail_template module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class WebAPI(metaclass=PoolMeta):
    'Email WebAPI'
    __name__ = 'email.webapi'

    smtp_conn = fields.Many2One('smtp.connection.mail', 'SMTP connection',
        states={
            'invisible': Eval('web_service') != 'smtp',
        })

    @classmethod
    def __setup__(cls):
        super(WebAPI, cls).__setup__()
        cls.web_service.selection.append(('smtp', 'SMTP'))

