# This file is part smtp module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import smtp
from . import email_
from . import template
from . import web_service


def register():
    Pool.register(
        smtp.SmtpConnectionMail,
        email_.Email,
        template.Template,
        web_service.WebAPI,
        module='smtp', type_='model')
